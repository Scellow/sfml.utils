﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace SFML.Utils.Spatial
{
    public interface ISpatialMap<T> : ICollection<T> where T : ISpatial
    {
        IEnumerable<T> Select(FloatRect region);
        IEnumerable<T> Select(Vector2f pos); 
    }
}