﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using SFML.Graphics;
using SFML.Window;
using System.Diagnostics;
using SFML.System;

namespace SFML.Utils
{
    public static class Misc
    {
        public static T Clamp<T>(this T val, T min, T max) where T : IComparable<T>
        {
            if (val.CompareTo(min) < 0) return min;
            else if (val.CompareTo(max) > 0) return max;
            else return val;
        }

        public static float Abs(this float value)
        {
            return Math.Abs(value);
        }

        public static void SetPoints(this ConvexShape shape, params Vector2f[] points)
        {
            shape.SetPointCount((uint)points.Length);
            for (int i = 0; i < points.Length; i++)
            {
                shape.SetPoint((uint)i, points[i]);
            }
            shape.SetPointCount((uint)points.Length);
        }

        [DllImport("csfml-graphics-2", CallingConvention = CallingConvention.Cdecl), SuppressUnmanagedCodeSecurity]
        unsafe static extern void sfTexture_updateFromPixels(IntPtr texture, byte* pixels, uint width, uint height, uint x, uint y);

        public static void Update(this Texture texture, Color[] pixels, IntRect rec)
        {

            Update(texture, pixels, (uint)rec.Width, (uint)rec.Height, (uint)rec.Left, (uint)rec.Top);
        }

        public unsafe static void Update(this Texture texture, byte* data, uint width, uint height, uint x, uint y)
        {
            sfTexture_updateFromPixels(texture.CPointer, data, width, height, x, y);
        }

        public static void Update(this Texture texture, Color[] pixels, uint width, uint height, uint x, uint y)
        {
            unsafe
            {
                fixed (Color* ptr = pixels)
                {
                    var bptr = (byte*)ptr;
                    sfTexture_updateFromPixels(texture.CPointer, bptr, width, height,x,y);
                }
            }
        }

        /// <summary>
        /// Assert that throw exception, instead of showing message box.
        /// </summary>
        [Conditional("DEBUG"), DebuggerHidden]
        public static void Assert(bool value, string message = "error")
        {
            if(!value) throw new Exception(message);
        }

        /// <summary>
        /// Calls Debugger.Break if value is true
        /// </summary>
        /// <param name="value"></param>
        [Conditional("DEBUG"), DebuggerHidden]
        public static void Break(bool value)
        {
            if (value) Debugger.Break();
        }

        [DebuggerHidden]
        public static void NotNull(object obj, string msg = "")
        {
            if(obj == null) throw new ArgumentNullException(msg);
        }

        public static void Measure(Action action, string label = null, int repeat =1)
        {
            label = label ?? action.ToString();
            var watch = Stopwatch.StartNew();
            var times = new double[repeat];
            for (int i = 0; i < times.Length; i++)
            {
                watch.Restart();
                action();
                times[i] = watch.Elapsed.TotalMilliseconds;
            }
            Array.Sort(times);
            Console.WriteLine("{0} : {1} ms", label, times[0]);
        }

        public static void Set(this RectangleShape shape, FloatRect rect)
        {
            shape.Position = rect.Position();
            shape.Scale = new Vector2f(1,1);
            shape.Size = rect.Size();
        }

        public static Color OpaqueMul(this Color color, float ratio)
        {
            ratio *= 255;
            color.R = (byte)(ratio * color.R / 255);
            color.G = (byte)(ratio * color.G / 255);
            color.B = (byte)(ratio * color.B / 255);
            return color;
        }
    }
            
}
